﻿using System;
using System.Text.RegularExpressions;

namespace IsDigit
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        static bool isDigit(char symbol) {
            Regex r = new Regex(@"([0-9]){1}$");
            
            return r.Match(symbol.ToString()).Success;
        }

    }
}
